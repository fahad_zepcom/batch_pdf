<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pdf', [ 'as' => 'pdf', 'uses' => 'Controller@printPDF']);
Route::get('/invoices', [ 'as' => 'invoices', 'uses' => 'Controller@createInvoice']);
Route::get('/email', [ 'as' => 'email', 'uses' => 'Controller@createEmail']);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/sendmail', function () {
    return         Mail::to('fahad.u@zep_com.com')->send(new \App\Mail\OrderShipped());

});