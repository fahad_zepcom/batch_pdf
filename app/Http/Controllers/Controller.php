<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Barryvdh\Snappy\Facades\SnappyImage as SnappyImage;
use App\Jobs\BatchPDF;
use App\Jobs\createInvoice;
use App\Jobs\createEmail;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\Job;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function printPDF()
    {
        $this->dispatch(new BatchPDF());
       // Queue::push(new BatchPDF());
       echo "Your requested has been initiated and is being processeed. Click <a href='/'>here</a> to return back.";
       $urls = \App\Urls::all();
       foreach ($urls as $url) {
        //echo $url->name;
        echo( $url->urls);
    }

    }

    public function createInvoice()
    {
        $this->dispatch(new createInvoice());
       echo "PDF Generation started!";
    }


    public function createEmail()
    {
        $job = (new createEmail())->onQueue('email');
        dispatch($job);
        echo "Emails are being sent!";
    }
    
}