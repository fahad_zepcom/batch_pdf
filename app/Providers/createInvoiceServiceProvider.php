<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Jobs\createInvoice;

class createInvoiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindMethod(createInvoice::class.'@handle', function ($job, $app) {
            return $job->handle($app->make(createInvoice::class));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
