<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class createEmailServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindMethod(createEmail::class.'@handle', function ($job, $app) {
            return $job->handle($app->make(createEmail::class));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
