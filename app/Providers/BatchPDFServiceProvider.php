<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Jobs\BatchPDF;

class BatchPDFServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindMethod(BatchPDF::class.'@handle', function ($job, $app) {
            return $job->handle($app->make(BatchPDF::class));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
