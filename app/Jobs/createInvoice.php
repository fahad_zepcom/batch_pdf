<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Barryvdh\Snappy\Facades\SnappyImage as SnappyImage;
use Illuminate\Database\Eloquent\Model;
use DB;


class createInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $results = DB::select( DB::raw("SELECT orders.id AS invoice_id, 
        orders.created_at AS order_time, 
        users.name AS user_name, 
        users.email AS user_email,
        users.id AS user_id, 
        products.name AS product_name, 
        products.price AS product_price
        FROM orders
        INNER JOIN users
        ON orders.uid = users.id
        INNER JOIN products
        ON orders.pid = products.id") );

        foreach ($results as $result) {
        // creates html template for Invoice and implements it
        $break= "<br>";
        $user_name=$result->user_name;
        $product_name=$result->product_name;
        $user_email=$result->user_email;
        $product_price=$result->product_price;
        $order_time=$result->order_time;
        $invoice_id= $result->invoice_id;
        $user_id= $result->user_id;
        $company_name="Zepcom";
        $title ="<h1 style='text-align:center;' > Invoice </h1> <div style='text-align:center;' >Invoice #: ".$invoice_id."</div>";
        $html="<html><head><style>
        table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 8px;
        }
        </style></head><body>".$title."<span style='float:left;'>From:<br>".$company_name."</span><span style='float:right;'>To:<br>".$user_name."</span>".$break."<div style='clear:both;'><br><br>The digital product you bought online has been sent to your email. Details are <br><br></div>
        
        <table>
  <tr>
    <th>Item</th>
    <th>Price</th>
    <th>Quantity</th>
  </tr>
  <tr>
    <td>".$product_name."</td>
    <td>".$product_price."</td>
    <td>1</td>
  </tr>
 
</table>
        
        "."</body></html>";
        Storage::disk('public')->put($invoice_id.'.pdf', PDF::loadHTML($html)->inline($html));
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
    }
}
