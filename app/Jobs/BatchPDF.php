<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Barryvdh\Snappy\Facades\SnappyImage as SnappyImage;
use Illuminate\Database\Eloquent\Model;
use DB;
class BatchPDF implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public static function handle()
    {
        // $urls = \App\Urls::all();
        // foreach ($urls as $url) {
        //     Storage::disk('public')->put($url->urls.'.pdf', PDF::loadFile($url->urls)->inline($url->urls));
        // }

        DB::table('urls')->orderBy('id')->chunk(10, function ($urls) {      //10 records at a time
            
            foreach ($urls as $url) {
                Storage::disk('public')->put($url->urls.'.pdf', PDF::loadFile($url->urls)->inline($url->urls));
            }
        });
    }
}