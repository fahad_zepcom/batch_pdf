<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

// use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
// use Barryvdh\Snappy\Facades\SnappyImage as SnappyImage;


class BatchPDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batch:pdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates batch pdfs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public $sites=["youtube.com","en.wikipedia.org","facebook.com","twitter.com","amazon.com","imdb.com","reddit.com","pinterest.com","ebay.com","tripadvisor.com","craigslist.org","walmart.com	","instagram.com","google.com","nytimes.com","apple.com","linkedin.com","indeed.com	","play.google.com","espn.com","webmd.com","cnn.com","homedepot.com","etsy.com","netflix.com","quora.com","microsoft.com","target.com","merriam-webster.com","forbes.com","mapquest.com","nih.gov","gamepedia.com","yahoo.com","healthline.com","foxnews.com","allrecipes.com","quizlet.com","weather.com","bestbuy.com","urbandictionary.com","mayoclinic.org","aol.com","genius.com","zillow.com","usatoday.com","glassdoor.com","msn.com","rottentomatoes.com","lowes.com","dictionary.com","businessinsider.com","usnews.com","medicalnewstoday.com","britannica.com","washingtonpost.com","usps.com","finance.yahoo.com","irs.gov","yellowpages.com","chase.com","retailmenot.com","accuweather.com","wayfair.com","go.com	","live.com","login.yahoo.com","steamcommunity.com","cnet.com","ign.com","steampowered.com","macys.com","wikihow.com","mail.yahoo.com","wiktionary.org","cbssports.com","cnbc.com","bankofamerica.com","expedia.com	","wellsfargo.com","groupon.com","twitch.tv","khanacademy.org","theguardian.com","paypal.com","spotify.com","att.com","nfl.com","realtor.com","ca.gov","goodreads.com","office.com","ufl.edu","mlb.com","foodnetwork.com","bbc.com","apartments.com","npr.org","wowhead.com"];

    public function __construct()
    {
        parent::__construct();
        
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // foreach ($this->sites as $url)
        // {
        //     //$this->handle($url);
        //     echo $url." ";
            
        // }
        
        $data = app()->call("Controller@printPDF");

       return ($data); 
    }
}
